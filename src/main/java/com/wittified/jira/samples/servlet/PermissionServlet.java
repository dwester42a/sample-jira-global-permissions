package com.wittified.jira.samples.servlet;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Scanned
public class PermissionServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(PermissionServlet.class);

    @ComponentImport
    private final GlobalPermissionManager globalPermissionManager;

    @ComponentImport
    private final JiraAuthenticationContext jiraAuthenticationContext;

    @Inject
    public PermissionServlet( final GlobalPermissionManager globalPermissionManager,
                              final JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.globalPermissionManager = globalPermissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");

        resp.getWriter().write("Current user has the permission: ");
        resp.getWriter().write( new Boolean( this.globalPermissionManager.hasPermission(
                GlobalPermissionKey.of("sample.custom.globalpermission")
                , this.jiraAuthenticationContext.getLoggedInUser())).toString());
        resp.getWriter().close();
    }

}